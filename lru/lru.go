package lru

import (
	"container/list"
	"sync"
	"time"
)

// Cache 是一个 LRU Cache
type Cache struct {
	lock sync.RWMutex
	//maxEntries 是 Cache 中實體的最大數量，0 表示沒有限制
	maxEntries int
	//ll是一個雙向鍊錶指針，執行一個 container/list 套件中的雙向鍊錶
	ll *list.List
	//cache 是一個 map，存放具體的 k/v 對，value 是雙向鍊錶中的具體元素，也就是 *Element
	cache map[interface{}]*list.Element
	// 過期時間
	expiredTime time.Duration
}

// Key 是接口，可以是任意型
type Key interface{}

// 一個 entry 包含一個 key 和一個 value，都是任意型別
type entry struct {
	key         Key
	value       interface{}
	expiredTime time.Time
}

func (e *entry) IsExpired() bool {
	return time.Now().After(e.expiredTime)
}

// New 建立一個 LRU Cache。 maxEntries 為 0 表示快取沒有大小限制
func New(maxEntries int, expiration time.Duration, isNeverExpired bool) *Cache {

	if isNeverExpired {
		// 注意：永不過期為限制十年
		expiration = 87600 * time.Hour
	} else {
		// 預設時間
		if expiration <= 0 {
			expiration = 10 * time.Minute
		}
	}

	return &Cache{
		maxEntries:  maxEntries,
		ll:          list.New(),
		cache:       make(map[interface{}]*list.Element),
		expiredTime: expiration,
	}
}

// SetAndUpdate 向 Cache 中插入一个 KV
func (c *Cache) SetAndUpdate(key Key, value interface{}) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if c.cache == nil {
		c.cache = make(map[interface{}]*list.Element)
		c.ll = list.New()
	}

	expire := time.Now().Add(c.expiredTime)

	if current, ok := c.cache[key]; ok {
		// 移到最前面
		c.ll.MoveToFront(current)
		current.Value.(*entry).expiredTime = expire
		return
	}

	// 元素加至最前
	new := c.ll.PushFront(&entry{key, value, expire})
	c.cache[key] = new
	if c.maxEntries != 0 && c.ll.Len() > c.maxEntries {
		c.removeOldest()
	}
}

// Get 傳入一個 key，傳回一個是否有該 key 以及對應 value
func (c *Cache) Get(key Key) (value interface{}, ok bool) {
	c.lock.RLock()
	defer c.lock.RUnlock()

	expire := time.Now().Add(c.expiredTime)

	if current, hit := c.cache[key]; hit {
		// 移到最前面
		c.ll.MoveToFront(current)
		current.Value.(*entry).expiredTime = expire
		return current.Value.(*entry).value, true
	}
	return
}

// 從 Cache 中刪除最久未被存取的數據
func (c *Cache) removeOldest() {
	// 獲取最後一個元素
	last := c.ll.Back()
	if last != nil {
		c.removeElement(last)
	}
}

// RemoveExpired 從 Cache 刪除過期的數據
func (c *Cache) RemoveExpired() {
	c.lock.Lock()
	defer c.lock.Unlock()

	for {
		// 獲取最後一個元素
		last := c.ll.Back()
		if last != nil {
			if last.Value.(*entry).IsExpired() {
				c.removeElement(last)
			} else {
				break
			}
		} else {
			break
		}
	}
}

// 從 Cache 中刪除一個元素，供內部調用
func (c *Cache) removeElement(e *list.Element) {
	//先從 list 中刪除
	c.ll.Remove(e)

	kv := e.Value.(*entry)

	//再從 map 中刪除
	delete(c.cache, kv.key)
}

// Len 取得 Cache 目前的元素個數
func (c *Cache) Len() int {
	if c.cache == nil {
		return 0
	}
	return c.ll.Len()
}

// Clear 清空 Cache
func (c *Cache) Clear() {
	c.ll = nil
	c.cache = nil
}
