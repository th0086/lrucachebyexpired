package localcache

import (
	"fmt"
	"time"

	"gitlab.com/th0086/lrucachebyexpired/lru"
)

type ILocalCache interface {
	SetAndUpdate(key interface{}, value interface{})
	Get(key interface{}) (value interface{}, ok bool)
	PeriodicallyClearExpired()
}

type localCache struct {
	cache     *lru.Cache
	name      string
	clearTime time.Duration
}

func NewCache() ILocalCache {
	return &localCache{
		cache:     lru.New(3, 5*time.Second, false),
		name:      "test",
		clearTime: 10 * time.Second,
	}
}

func (lc *localCache) SetAndUpdate(key interface{}, value interface{}) {
	lc.cache.SetAndUpdate(key, value)
}

func (lc *localCache) Get(key interface{}) (value interface{}, ok bool) {
	return lc.cache.Get(key)
}

// 執行有需要定時清理的cache
func (lc *localCache) PeriodicallyClearExpired() {
	go func(cache *lru.Cache) {
		defer fmt.Errorf("cache:[%s]:An unknown error occurred", lc.name)
		tick := time.Tick(lc.clearTime)
		for {
			select {
			case <-tick:
				cache.RemoveExpired()
			}
		}
	}(lc.cache)
}
