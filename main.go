package main

import (
	"fmt"
	"time"

	"gitlab.com/th0086/lrucachebyexpired/localcache"
)

func main() {

	cache := localcache.NewCache()

	cache.PeriodicallyClearExpired()

	cache.SetAndUpdate("alan", 18)
	cache.SetAndUpdate("peggy", 18)
	cache.SetAndUpdate("jason", 30)
	v, ok := cache.Get("alan")
	if ok {
		fmt.Printf("alan's age is %v\n", v)
	} else {
		fmt.Printf("alan was evicted out\n")
	}

	cache.SetAndUpdate("vic", 50)

	v, ok = cache.Get("jason")
	if ok {
		fmt.Printf("jason's age is %v\n", v)
	} else {
		fmt.Printf("jason was evicted out\n")
	}

	v, ok = cache.Get("peggy")
	if ok {
		fmt.Printf("peggy's age is %v\n", v)
	} else {
		fmt.Printf("peggy was evicted out\n")
	}

	fmt.Println("-----------------------------")
	time.Sleep(3 * time.Second)

	cache.SetAndUpdate("david", 40)

	fmt.Println("-----------------------------")
	time.Sleep(3 * time.Second)

	v, ok = cache.Get("alan")
	if ok {
		fmt.Printf("alan's age is %v\n", v)
	} else {
		fmt.Printf("alan was evicted out\n")
	}

	v, ok = cache.Get("vic")
	if ok {
		fmt.Printf("vic's age is %v\n", v)
	} else {
		fmt.Printf("vic was evicted out\n")
	}

	v, ok = cache.Get("david")
	if ok {
		fmt.Printf("david's age is %v\n", v)
	} else {
		fmt.Printf("david was evicted out\n")
	}

	fmt.Println("-----------------------------")
	time.Sleep(6 * time.Second)

	v, ok = cache.Get("david")
	if ok {
		fmt.Printf("david's age is %v\n", v)
	} else {
		fmt.Printf("david was evicted out\n")
	}
}
